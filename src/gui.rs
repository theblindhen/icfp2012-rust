#![allow(dead_code)]
#![allow(unused_variables)]
#![allow(unused_imports)]

mod map;
mod model;
mod sim;
mod util;

use map::{Cell, Map};
use model::Command;
use sim::GameEnded::*;
use sim::State;
use sim::StepResult::*;
use util::{Coord, Dir};

use std::collections::HashMap;
use std::fs;
use std::io::{self, Read};
use std::path::PathBuf;
use structopt::StructOpt;

extern crate image;

use image::*;

/// Global consts and variables
const TILE_WIDTH: u32 = 16;
const TILE_HEIGHT: u32 = 16;

/// Main
#[derive(StructOpt, Debug)]
#[structopt()]
struct Opt {
    // The number of occurrences of the `v/verbose` flag
    /// Verbose mode (-v, -vv, -vvv, etc.)
    #[structopt(short = "v", long = "verbose", parse(from_occurrences))]
    verbose: u8,

    /// Input file
    #[structopt(name = "MAP", parse(from_os_str))]
    file: PathBuf,
}

fn load_tiles() -> HashMap<Cell, image::DynamicImage> {
    let mut tiles = HashMap::new();
    let cells = [
        (Cell::Robot, "tiles/miner_f.png"),
        (Cell::Wall, "tiles/bricks_f.png"),
        (Cell::Rock, "tiles/rock_f.png"),
        (Cell::Lambda, "tiles/lambda_f.png"),
        (Cell::ClosedLift, "tiles/lift_f.png"),
        (Cell::OpenLift, "tiles/openlift_f.png"),
        (Cell::Earth, "tiles/earth_f.png"),
        (Cell::Empty, "tiles/empty_f.png"),
    ];
    for (cell, file) in cells.iter() {
        let img = image::open(*file).unwrap();
        tiles.insert(*cell, img);
    }
    tiles
}

fn dump_state_image(state: &State, tiles: &HashMap<Cell, image::DynamicImage>) {
    let map = state.get_map();
    // Create a new ImgBuf with width: imgx and height: imgy
    let mut imgbuf =
        image::ImageBuffer::new(TILE_WIDTH * map.cols as u32, TILE_HEIGHT * map.rows as u32);

    for y in (1..map.rows + 1).rev() {
        for x in 1..map.cols + 1 {
            let cell = map[Coord { x, y }];
            GenericImage::copy_from(
                &mut imgbuf,
                tiles.get(&cell).unwrap(),
                16 * (x - 1) as u32,
                16 * (map.rows - y) as u32,
            )
            .unwrap();
        }
    }

    // Write the contents of this image to the Writer in PNG format.
    imgbuf
        .save(format!("gui/step_{:03}.png", state.get_moves()))
        .unwrap();
}

fn main() -> std::io::Result<()> {
    let opt = Opt::from_args();
    let map_string = fs::read_to_string(opt.file)?;
    let map = Map::load(&map_string);
    let mut state = State::from_map(map);
    let tiles = load_tiles();

    loop {
        println!("{}", state.to_string());
        dump_state_image(&state, &tiles);

        println!("select direction");
        let mut dir = String::new();
        io::stdin().read_line(&mut dir).unwrap();

        let command = match dir.chars().next().unwrap() {
            'R' => Command::Move(Dir::Right),
            'L' => Command::Move(Dir::Left),
            'U' => Command::Move(Dir::Up),
            'D' => Command::Move(Dir::Down),
            'W' => Command::Wait,
            'A' => Command::Abort,
            _ => {
                println!("Not a direction, ye olde kernel panics!");
                continue;
            }
        };

        match sim::step(&state, command) {
            Moved(next_state) => state = next_state,
            Ended(MineCompleted(score)) | Ended(Aborted(score)) => {
                println!("Game ended with score: {}", score);
                break;
            }
            Ended(Destroyed) => {
                println!("Robot was destroyed");
                break;
            }
        }
    }

    Ok(())
}
