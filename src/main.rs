#![allow(dead_code)]
#![allow(unused_variables)]

mod astar;
mod map;
mod model;
mod sim;
mod solver;
mod util;

use map::Map;
use model::Command;
use sim::State;
use util::Dir;

use sim::GameEnded::*;
use sim::StepResult::*;
use std::fs;
use std::io;
use std::path::PathBuf;
use structopt::StructOpt;

use std::thread;
use std::time::Duration;

use simplelog::*;
use solver::Solver;

use std::marker::PhantomData;

use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;

/// Main
#[derive(StructOpt, Debug)]
#[structopt()]
struct Opt {
    // The number of occurrences of the `v/verbose` flag
    /// Verbose mode (-v, -vv, -vvv, etc.)
    #[structopt(short = "v", long = "verbose", parse(from_occurrences))]
    verbose: u8,

    /// Run in interactive mode
    #[structopt(short, long)]
    interactive: bool,

    #[structopt(default_value = "1000", short, long)]
    timeout: u32,

    /// Input file
    #[structopt(name = "MAP", parse(from_os_str))]
    file: PathBuf,
}

fn interactive(mut state: State) {
    loop {
        println!("{}", state.to_string());
        println!("select direction");

        let mut dir = String::new();
        io::stdin().read_line(&mut dir).unwrap();

        let dir = match dir.chars().next().unwrap() {
            'R' => Dir::Right,
            'L' => Dir::Left,
            'U' => Dir::Up,
            'D' => Dir::Down,
            _ => panic!("Unknown direction"),
        };

        match sim::step(&state, Command::Move(dir)) {
            Moved(next_state) => state = next_state,
            Ended(MineCompleted(score)) | Ended(Aborted(score)) => {
                println!("Game ended with score: {}", score);
                break;
            }
            Ended(Destroyed) => {
                println!("Robot was destroyed");
                break;
            }
        }
    }
}

fn main() -> std::io::Result<()> {
    let opt = Opt::from_args();
    let level_filter = match opt.verbose {
        1 => LevelFilter::Debug,
        2 => LevelFilter::Trace,
        _ => LevelFilter::Info,
    };

    CombinedLogger::init(vec![TermLogger::new(
        level_filter,
        Config::default(),
        TerminalMode::Mixed,
    )
    .unwrap()])
    .unwrap();

    let map_string = fs::read_to_string(opt.file)?;
    let map = Map::load(&map_string);
    log::info!("Loaded {}x{} map", map.rows, map.cols);

    let state = State::from_map(map);

    if opt.interactive {
        interactive(state)
    } else {
        log::trace!("Starting state:\n{}", &state);
        let stop = Arc::new(AtomicBool::new(false));

        let solvers = vec![
                ("A*", Box::new(solver::AStarSolver::new(stop.clone())) as Box<dyn Solver>),
                ("GotoNearest", Box::new(solver::GotoNearestSolver::<solver::AStarGotoSolver>::new(stop.clone(), PhantomData))),
            ];

        let mut solver_threads = Vec::new();
        for (name, solver) in solvers {
            let init_state = state.clone();
            solver_threads.push((name, thread::spawn(move || { solver.solve(init_state) })));
        }

        let timeout = opt.timeout.into();

        thread::spawn(move || {
            thread::sleep(Duration::from_millis(timeout));
            stop.store(true, Ordering::Release);
        });

        for (name, solver_thread) in solver_threads {
            let path = solver_thread.join().unwrap();

            match path {
                Some(commands) => {
                    let mut score = -1;
                    let desc;
                    match sim::multistep(&state, &commands) {
                        Moved(_) => desc = "Incomplete solution",
                        Ended(MineCompleted(s)) => {
                            desc = "Mine solved";
                            score = s;
                        }
                        Ended(_) => desc = "Incorrect solution",
                    }
                    log::info!("{}: {}. Length: {}. Score: {}.", name, desc, commands.len(), score);
                    log::info!("{}: Solution: {}", name, model::format_cmds(&commands));
                }
                None => log::info!("{}: No solution found :(", name),
            }
        }

        ()
    }

    Ok(())
}
