#![allow(dead_code)]

use crate::util::Coord;
use std::ops::{Index, IndexMut};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Cell {
    Robot,
    Wall,
    Rock,
    Lambda,
    ClosedLift,
    OpenLift,
    Earth,
    Empty,
}

impl Cell {
    pub fn from_char(c: char) -> Self {
        match c {
            'R' => Cell::Robot,
            '#' => Cell::Wall,
            '*' => Cell::Rock,
            '\\' | 'λ' => Cell::Lambda,
            'O' => Cell::OpenLift,
            'L' => Cell::ClosedLift,
            '.' => Cell::Earth,
            ' ' => Cell::Empty,
            _ => panic!("Unknown map character"),
        }
    }

    pub fn to_char(self) -> char {
        match self {
            Cell::Robot => 'R',
            Cell::Wall => '#',
            Cell::Rock => '*',
            Cell::Lambda => '\\',
            Cell::OpenLift => 'O',
            Cell::ClosedLift => 'L',
            Cell::Earth => '.',
            Cell::Empty => ' ',
        }
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub struct Map {
    pub rows: i32,
    pub cols: i32,
    pub cells: Vec<Cell>,
}

impl Map {
    pub fn load(map_str: &str) -> Self {
        let cols = map_str
            .lines()
            .map(|line| line.len())
            .max()
            .expect("Couldn't determine number of cols in map");
        let rows = map_str.lines().count();

        let mut cells = vec![Cell::Wall; cols * rows];

        for (i, line) in map_str.lines().enumerate() {
            for (j, c) in line.chars().enumerate() {
                cells[(rows - i - 1) * cols + j] = Cell::from_char(c);
            }
        }

        Map {
            rows: rows as i32,
            cols: cols as i32,
            cells: cells,
        }
    }

    pub fn idx_of_coord(&self, c: Coord) -> i32 {
        assert!(c.x >= 1);
        assert!(c.y >= 1);
        assert!(c.x <= self.cols);
        assert!(c.y <= self.rows);
        (c.x - 1) + self.cols * (c.y - 1)
    }

    pub fn coord_of_idx(&self, idx: i32) -> Coord {
        let y = idx / self.cols;
        let x = idx - (y * self.cols);
        Coord { x: x + 1, y: y + 1 }
    }
}

impl<'a> IntoIterator for &'a Map {
    type Item = (Coord, Cell);
    type IntoIter = MapIterator<'a>;

    fn into_iter(self) -> Self::IntoIter {
        MapIterator { idx: 0, map: self }
    }
}

pub struct MapIterator<'a> {
    idx: usize,
    map: &'a Map,
}

impl<'a> Iterator for MapIterator<'a> {
    type Item = (Coord, Cell);

    fn next(&mut self) -> Option<Self::Item> {
        let res = self
            .map
            .cells
            .get(self.idx)
            .map(|cell| (self.map.coord_of_idx(self.idx as i32), *cell));
        self.idx += 1;
        res
    }
}

impl Index<Coord> for Map {
    type Output = Cell;

    fn index(&self, c: Coord) -> &Self::Output {
        &self.cells[self.idx_of_coord(c) as usize]
    }
}

impl Index<(i32, i32)> for Map {
    type Output = Cell;

    fn index(&self, (x, y): (i32, i32)) -> &Self::Output {
        &self.cells[self.idx_of_coord(Coord { x, y }) as usize]
    }
}

impl IndexMut<Coord> for Map {
    fn index_mut(&mut self, c: Coord) -> &mut Self::Output {
        let idx = self.idx_of_coord(c) as usize;
        &mut self.cells[idx]
    }
}

impl IndexMut<(i32, i32)> for Map {
    fn index_mut(&mut self, (x, y): (i32, i32)) -> &mut Self::Output {
        let idx = self.idx_of_coord(Coord { x, y }) as usize;
        &mut self.cells[idx]
    }
}

impl std::fmt::Display for Map {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut row = String::new();

        for y in (1..self.rows + 1).rev() {
            for x in 1..self.cols + 1 {
                let coord = Coord { x, y };
                row.push(self[coord].to_char());
            }
            write!(f, "{}\n", row)?;
            row.clear();
        }

        Ok(())
    }
}
