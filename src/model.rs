use crate::util::Dir;
use std::fmt;

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Command {
    Move(Dir),
    Wait,
    Abort,
}

impl fmt::Display for Command {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Command::Move(Dir::Left) => 'L',
                Command::Move(Dir::Right) => 'R',
                Command::Move(Dir::Up) => 'U',
                Command::Move(Dir::Down) => 'D',
                Command::Wait => 'W',
                Command::Abort => 'A',
            }
        )
    }
}

pub fn format_cmds(cmds: &Vec<Command>) -> String {
    cmds.iter()
        .map(|comm| format!("{}", comm))
        .collect::<String>()
}
