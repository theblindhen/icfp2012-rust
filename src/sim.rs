use crate::map::{Cell, Map};
use crate::model::Command;
#[allow(unused_variables)]
use crate::util::{Coord, Dir};
use std::collections::HashSet;

#[derive(PartialEq, Eq, Hash, Clone)]
pub struct State {
    map: Map,
    pos: Coord,
    moves: i32,
    lambdas: i32,
    lambda_coords: Vec<Coord>, // Ugly, we use it as a set but that doesn't hash
    lambda_lift: Coord,
    rocks: Vec<Coord>,
}

impl State {
    pub fn from_map(map: Map) -> Self {
        let pos = map.coord_of_idx(
            map.cells
                .iter()
                .position(|&x| x == Cell::Robot)
                .expect("Could not determine position of robot") as i32,
        );

        let rocks = map
            .into_iter()
            .filter(|(c, cell)| *cell == Cell::Rock)
            .map(|(c, _)| c)
            .collect();
        let lambda_lift = {
            let (c, _) = map
                .into_iter()
                .find(|(c, cell)| *cell == Cell::ClosedLift)
                .expect("No closed lift in map");
            c
        };
        let lambda_coords = map
            .into_iter()
            .filter(|(c, cell)| *cell == Cell::Lambda)
            .map(|(c, _)| c)
            .collect();
        State {
            map,
            pos,
            moves: 0,
            lambdas: 0,
            lambda_lift,
            rocks,
            lambda_coords,
        }
    }

    pub fn get_map(&self) -> &Map {
        &self.map
    }

    pub fn get_pos(&self) -> Coord {
        self.pos
    }

    pub fn get_moves(&self) -> i32 {
        self.moves
    }

    pub fn get_lambdas_left(&self) -> i32 {
        self.lambda_coords.len() as i32
    }

    pub fn get_lambdas_coords(&self) -> Vec<Coord> {
        self.lambda_coords.clone()
    }

    pub fn get_lift_coord(&self) -> Coord {
        self.lambda_lift
    }
}

impl std::fmt::Display for State {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Map:\n{}", self.map)?;
        writeln!(f, "Position: {}", self.pos)?;
        writeln!(f, "Moves: {}", self.moves)?;
        writeln!(f, "Lambdas: {}", self.lambdas)?;
        writeln!(f, "Lambdas left: {}", self.get_lambdas_left())?;

        Ok(())
    }
}

pub enum GameEnded {
    MineCompleted(i32),
    Aborted(i32),
    Destroyed,
}

pub fn is_move_valid(state: &State, dir: Dir) -> bool {
    let next_pos = state.pos + dir;
    match state.map[next_pos] {
        Cell::Empty | Cell::Earth | Cell::Lambda | Cell::OpenLift => true,
        Cell::Rock => {
            (dir == Dir::Left || dir == Dir::Right) && (state.map[next_pos + dir] == Cell::Empty)
        }
        _ => false,
    }
}

pub enum StepResult {
    Moved(State),
    Ended(GameEnded),
}

fn move_robot(state: &State, dir: Dir) -> StepResult {
    let next_pos = state.pos + dir;
    let next_cell = state.map[next_pos];
    match next_cell {
        Cell::OpenLift => {
            StepResult::Ended(GameEnded::MineCompleted(state.lambdas * 75 - state.moves))
        }
        Cell::Lambda | Cell::Empty | Cell::Earth => {
            let mut next_state = state.clone();
            next_state.moves += 1;
            next_state.pos = next_pos;
            next_state.map[next_pos] = Cell::Robot;
            next_state.map[state.pos] = Cell::Empty;

            if next_cell == Cell::Lambda {
                next_state.lambdas += 1;
                next_state.lambda_coords = next_state
                    .lambda_coords // Vec::remove_item in nightly
                    .into_iter()
                    .filter(|&c| c != next_pos)
                    .collect();
            }

            StepResult::Moved(next_state)
        }
        Cell::Rock
            if (dir == Dir::Left || dir == Dir::Right)
                && (state.map[next_pos + dir] == Cell::Empty) =>
        {
            let mut next_state = state.clone();
            next_state.moves += 1;
            next_state.pos = next_pos;
            next_state.map[next_pos] = Cell::Robot;
            next_state.map[state.pos] = Cell::Empty;
            next_state.map[next_pos + dir] = Cell::Rock;
            next_state.rocks = next_state
                .rocks
                .into_iter()
                .filter(|&c| c != next_pos)
                .collect();
            next_state.rocks.push(next_pos + dir);

            StepResult::Moved(next_state)
        }
        _ => {
            let mut next_state = state.clone();
            next_state.moves += 1;

            StepResult::Moved(next_state)
        }
    }
}

fn update_after_command(state: &State) -> State {
    // TODO: Avoid full clone here:
    // This requires that we own input state
    // That entails that did_we_die does not need a previous state
    let mut new = state.clone();
    let mut new_rocks = HashSet::new();

    //Don't need to sort coordinates since reads are from old state and writes are to new
    // new.rocks
    //     .sort_by(|c1,c2| match c1.y.partial_cmp(&c2.y) {
    //         Some(Ordering::Equal) => c1.x.partial_cmp(&c2.x).unwrap(),
    //         Some(order) => order,
    //         _ => panic!("Unorderable rocks")
    //             });
    for coord in state.rocks.iter() {
        let (x, y) = (coord.x, coord.y);
        if state.map[(x, y)] != Cell::Rock {
            panic!("Not a rock in a rock's place!");
        }
        // Fall straight down
        if state.map[(x, y - 1)] == Cell::Empty {
            new.map[(x, y)] = Cell::Empty;
            new.map[(x, y - 1)] = Cell::Rock;
            new_rocks.insert(Coord { x, y: y - 1 });

        // Slide right off rock
        } else if state.map[(x, y - 1)] == Cell::Rock
            && state.map[(x + 1, y)] == Cell::Empty
            && state.map[(x + 1, y - 1)] == Cell::Empty
        {
            new.map[(x, y)] = Cell::Empty;
            new.map[(x + 1, y - 1)] = Cell::Rock;
            new_rocks.insert(Coord { x: x + 1, y: y - 1 });

        // Slide left off rock
        } else if state.map[(x, y - 1)] == Cell::Rock
            && state.map[(x - 1, y)] == Cell::Empty
            && state.map[(x - 1, y - 1)] == Cell::Empty
        {
            new.map[(x, y)] = Cell::Empty;
            new.map[(x - 1, y - 1)] = Cell::Rock;
            new_rocks.insert(Coord { x: x - 1, y: y - 1 });

        // Slide right off lambda
        } else if state.map[(x, y - 1)] == Cell::Lambda
            && state.map[(x + 1, y)] == Cell::Empty
            && state.map[(x + 1, y - 1)] == Cell::Empty
        {
            new.map[(x, y)] = Cell::Empty;
            new.map[(x + 1, y - 1)] = Cell::Rock;
            new_rocks.insert(Coord { x: x + 1, y: y - 1 });
        } else {
            new_rocks.insert(*coord);
        }
    }
    new.rocks = new_rocks.into_iter().collect();

    if state.get_lambdas_left() == 0 && state.get_map()[state.lambda_lift] == Cell::ClosedLift {
        new.map[state.lambda_lift] = Cell::OpenLift;
    }

    new
}

fn did_we_lose(prev_state: &State, next_state: &State) -> bool {
    let above = next_state.pos + Dir::Up;
    next_state.map[above] == Cell::Rock && prev_state.map[above] != Cell::Rock
}

pub fn step(state: &State, command: Command) -> StepResult {
    match command {
        Command::Wait => {
            let new_state = update_after_command(&state);
            if did_we_lose(state, &new_state) {
                StepResult::Ended(GameEnded::Destroyed)
            } else {
                StepResult::Moved(new_state)
            }
        }
        Command::Move(dir) => match move_robot(state, dir) {
            StepResult::Moved(state_after_move) => {
                let new_state = update_after_command(&state_after_move);
                if did_we_lose(&state_after_move, &new_state) {
                    StepResult::Ended(GameEnded::Destroyed)
                } else {
                    StepResult::Moved(new_state)
                }
            }
            ended @ StepResult::Ended(_) => ended,
        },
        Command::Abort => StepResult::Ended(GameEnded::Aborted(50 * state.lambdas - state.moves)),
    }
}

pub fn multistep(state: &State, commands: &Vec<Command>) -> StepResult {
    let mut state = state.clone();

    for &command in commands.iter() {
        match step(&state, command) {
            StepResult::Moved(new_state) => {
                log::trace!("{}", new_state);
                state = new_state;
            }
            res => {
                return res;
            }
        }
    }

    return StepResult::Moved(state);
}

#[cfg(test)]
mod tests {
    use super::*;

    fn assert_map_after_step(command: Command, map_before_str: &str, map_after_str: &str) {
        let map_before = Map::load(&map_before_str);
        let map_target = Map::load(&map_after_str);
        let state = State::from_map(map_before);
        match step(&state, command) {
            StepResult::Moved(new_state) => {
                // println!("{}", new_state.to_string());
                assert_eq!(new_state.get_map(), &map_target);
            }
            _ => panic!("Cannot perform command"),
        }
    }

    #[test]
    fn test_rock_falls_down() {
        assert_map_after_step(
            Command::Wait,
            r#"
                               #####
                               #R L#
                               #***#
                               #* *#
                               #####"#,
            r#"
                               #####
                               #R O#
                               #* *#
                               #***#
                               #####"#,
        );
    }

    #[test]
    fn test_rock_falls_right() {
        assert_map_after_step(
            Command::Wait,
            "#####\n\
             #R L#\n\
             # * #\n\
             # * #\n\
             #####",
            "#####\n\
             #R O#\n\
             #   #\n\
             # **#\n\
             #####",
        );
    }

    #[test]
    fn test_rock_falls_left1() {
        assert_map_after_step(
            Command::Wait,
            "#####\n\
             #R L#\n\
             # **#\n\
             # * #\n\
             #####",
            "#####\n\
             #R O#\n\
             #   #\n\
             #***#\n\
             #####",
        );
    }

    #[test]
    fn test_rock_falls_left2() {
        assert_map_after_step(
            Command::Wait,
            "#####\n\
             #R L#\n\
             # * #\n\
             # **#\n\
             #####",
            "#####\n\
             #R O#\n\
             #   #\n\
             #***#\n\
             #####",
        );
    }

    #[test]
    fn test_rock_falls_left3() {
        assert_map_after_step(
            Command::Wait,
            "#####\n\
             #R L#\n\
             # * #\n\
             # **#\n\
             #####",
            "#####\n\
             #R O#\n\
             #   #\n\
             #***#\n\
             #####",
        );
    }

    #[test]
    fn test_rock_falls_right_on_lambda() {
        assert_map_after_step(
            Command::Wait,
            "#####\n\
             #R L#\n\
             # * #\n\
             # λ #\n\
             #####",
            "#####\n\
             #R L#\n\
             #   #\n\
             # λ*#\n\
             #####",
        );
    }

    #[test]
    fn test_rock_stays_left_on_lambda1() {
        assert_map_after_step(
            Command::Wait,
            "#####\n\
             #R L#\n\
             # **#\n\
             # λ #\n\
             #####",
            "#####\n\
             #R L#\n\
             # * #\n\
             # λ*#\n\
             #####",
        );
    }

    #[test]
    fn test_rock_stays_left_on_lambda2() {
        assert_map_after_step(
            Command::Wait,
            "#####\n\
             #R L#\n\
             # * #\n\
             # λ*#\n\
             #####",
            "#####\n\
             #R L#\n\
             # * #\n\
             # λ*#\n\
             #####",
        );
    }

    #[test]
    fn test_crashing_rocks1() {
        assert_map_after_step(
            Command::Wait,
            "#####\n\
             #R L#\n\
             #* *#\n\
             #* *#\n\
             #####",
            "#####\n\
             #R O#\n\
             #   #\n\
             #***#\n\
             #####",
        );
    }

    #[test]
    fn test_crashing_rocks2() {
        assert_map_after_step(
            Command::Wait,
            "######\n\
             #   L#\n\
             #* * #\n\
             #* *R#\n\
             ######",
            "######\n\
             #   O#\n\
             #    #\n\
             #***R#\n\
             ######",
        );
    }

    #[test]
    fn test_multi_fall() {
        assert_map_after_step(
            Command::Wait,
            "######\n\
             #*  L#\n\
             #*   #\n\
             #   R#\n\
             ######",
            "######\n\
             #   O#\n\
             # *  #\n\
             #*  R#\n\
             ######",
        );
    }

    #[test]
    fn test_robot_moves_rock() {
        assert_map_after_step(
            Command::Move(Dir::Left),
            "######\n\
             #   L#\n\
             #  *R#\n\
             ######",
            "######\n\
             #   O#\n\
             # *R #\n\
             ######",
        );
    }
}
