use crate::map::Cell;
use crate::model::Command;
use crate::sim::{is_move_valid, step, GameEnded, State, StepResult};
use crate::util::Dir::*;
use std::cmp::{max, min};

use crate::astar;
use crate::util;
use crate::sim;
use crate::model;

use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::marker::PhantomData;

#[derive(Clone)]
enum Node {
    // The command is how to get to the state (first element in path is None)
    Active(State, Option<Command>),
    Done(Command),
}

impl Node {
    fn is_done(&self) -> bool {
        match self {
            Node::Done(_) => true,
            Node::Active(_, _) => false,
        }
    }

    fn robot_is_at(&self, pos: util::Coord) -> bool {
        match self {
            Node::Done(_) => false,
            Node::Active(s, _) => s.get_pos() == pos
        }
    }
}

impl PartialEq for Node {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Node::Done(_), Node::Done(_)) => true,
            (Node::Active(s1, _), Node::Active(s2, _)) => {
                s1.get_pos() == s2.get_pos() && s1.get_map() == s2.get_map()
            }
            _ => false,
        }
    }
}

impl std::hash::Hash for Node {
    fn hash<H: std::hash::Hasher>(&self, hash_state: &mut H) {
        match self {
            Node::Done(_) => 0.hash(hash_state),
            Node::Active(state, _) => state.get_map().hash(hash_state),
        }
    }
}

impl Eq for Node {}

impl Node {
    fn successors(&self) -> Vec<(Node, i32)> {
        match self {
            Node::Active(state, _) => vec![Left, Right, Up, Down]
                .into_iter()
                .filter(|dir| is_move_valid(state, *dir))
                .map(|dir| {
                    let c = Command::Move(dir);
                    (step(state, c), c)
                })
                .filter_map(|(result, comm)| match result {
                    StepResult::Moved(new_state) => Some((Node::Active(new_state, Some(comm)), 1)),
                    StepResult::Ended(GameEnded::MineCompleted(_)) => Some((Node::Done(comm), 1)),
                    StepResult::Ended(GameEnded::Aborted(_)) => None,
                    StepResult::Ended(GameEnded::Destroyed) => None,
                })
                .collect(),
            Node::Done(_) => panic!("Broken AI"),
        }
    }

    fn lambda_heuristic(&self) -> i32 {
        match self {
            Node::Active(state, _) => state.get_lambdas_left(),
            Node::Done(_) => 0,
        }
    }

    fn bound_heuristic(&self) -> i32 {
        match self {
            Node::Active(state, _) => {
                let map = state.get_map();

                let mut xmin = map.cols;
                let mut xmax = 0;
                let mut ymin = map.rows;
                let mut ymax = 0;

                for (c, cell) in map {
                    match cell {
                        Cell::Robot | Cell::Lambda | Cell::OpenLift | Cell::ClosedLift => {
                            xmin = min(c.x, xmin);
                            ymin = min(c.y, ymin);
                            xmax = max(c.x, xmax);
                            ymax = max(c.y, ymax);
                        }
                        Cell::Earth | Cell::Empty | Cell::Rock | Cell::Wall => {}
                    }
                }

                (xmax - xmin) + (ymax - ymin)
            }
            Node::Done(_) => 0,
        }
    }

    fn better_bound_heuristic(&self) -> i32 {
        match self {
            Node::Active(state, _) => {
                let map = state.get_map();

                let mut xmin = map.cols;
                let mut xmax = 0;
                let mut ymin = map.rows;
                let mut ymax = 0;

                let robot = state.get_pos();
                let mut exit = state.get_pos();

                for (c, cell) in map {
                    match cell {
                        Cell::Robot | Cell::Lambda | Cell::OpenLift | Cell::ClosedLift => {
                            xmin = min(c.x, xmin);
                            ymin = min(c.y, ymin);
                            xmax = max(c.x, xmax);
                            ymax = max(c.y, ymax);

                            if cell == Cell::OpenLift || cell == Cell::ClosedLift {
                                exit = c
                            }
                        }
                        Cell::Earth | Cell::Empty | Cell::Rock | Cell::Wall => {}
                    }
                }

                (xmax - xmin) * 2 - (max(robot.x, exit.x) - min(robot.x, exit.x))
                    + (ymax - ymin) * 2
                    - (max(robot.y, exit.y) - min(robot.y, exit.y))
            }
            Node::Done(_) => 0,
        }
    }

    fn dist_to(&self, dst: util::Coord) -> i32 {
        match self {
            Node::Active(s, _) => {
                s.get_pos().dist_to(dst)
            },
            Node::Done(_) => 0,
        }
    }
}

pub trait Solver : Send + Sync {
    fn solve(&self, state: State) -> Option<Vec<Command>>;
}

pub trait GotoSolver : Solver {
    fn new(stop: Arc<AtomicBool>, dst: util::Coord) -> Self;
}

pub struct AStarSolver {
    stop: Arc<AtomicBool>,
}

impl AStarSolver {
    pub fn new(stop: Arc<AtomicBool>) -> Self {
        AStarSolver { stop }
    }
}

impl Solver for AStarSolver {
    fn solve(&self, state: State) -> Option<Vec<Command>> {
        let stop = &self.stop;
        let (path, info) = astar::astar(
            &Node::Active(state, None),
            |n| n.successors(),
            |n| n.better_bound_heuristic(),
            |n| n.is_done(),
            |_| stop.load(Ordering::Acquire),
        );
        log::info!(
            "A*: Visited {} nodes in {}ms",
            info.nodes_visited,
            info.execution_time.as_millis()
        );
        path.map(|path| {
            path.into_iter()
                .filter_map(|node| match node {
                    Node::Active(state, Some(comm)) => Some(comm),
                    Node::Done(comm) => Some(comm),
                    Node::Active(_, None) => None, // Ignore turn 0
                })
                .collect()
        })
    }
}

pub struct AStarGotoSolver {
    stop: Arc<AtomicBool>,
    dst: util::Coord,
}

impl GotoSolver for AStarGotoSolver {
    fn new(stop: Arc<AtomicBool>, dst: util::Coord) -> Self {
        AStarGotoSolver {
            stop,
            dst
        }
    }
}

impl Solver for AStarGotoSolver {
    fn solve(&self, state: State) -> Option<Vec<Command>> {
        let stop = &self.stop;
        let dst = self.dst;
        let (path, info) = astar::astar(
            &Node::Active(state, None),
            |n| n.successors(),
            |n| n.dist_to(dst),
            |n| n.is_done() || n.robot_is_at(dst),
            |_| stop.load(Ordering::Acquire),
        );
        log::info!(
            "A* to {}: Visited {} nodes in {}ms",
            dst,
            info.nodes_visited,
            info.execution_time.as_millis()
        );
        path.map(|path| {
            path.into_iter()
                .filter_map(|node| match node {
                    Node::Active(state, Some(comm)) => Some(comm),
                    Node::Done(comm) => Some(comm),
                    Node::Active(_, None) => None, // Ignore turn 0
                })
                .collect()
        })
    }
}

pub struct GotoNearestSolver<A: GotoSolver> {
    stop: Arc<AtomicBool>,
    solver: PhantomData<A>
}

impl<A: GotoSolver> GotoNearestSolver<A> {
    pub fn new(stop: Arc<AtomicBool>, solver: PhantomData<A>) -> Self {
        GotoNearestSolver { stop, solver }
    }
}

impl<A: GotoSolver> Solver for GotoNearestSolver<A> {
    fn solve(&self, state: State) -> Option<Vec<Command>> {
        let stop = &self.stop;
        let mut current_state = state;
        let mut current_path = Vec::new();
        let mut lambdas_left = current_state.get_lambdas_coords();
        let mut robot_coord = current_state.get_pos();
        while lambdas_left.len() > 0 {
            let (dst, _) = 
                lambdas_left.iter().map(|c| (c, robot_coord.dist_to(*c))).min_by_key(|(c, dist)| *dist).unwrap();

            log::debug!("GotoNearest: Robot is at {}, next target is {}", robot_coord, dst);

            let local_solver = A::new(stop.clone(), *dst);
            match local_solver.solve(current_state.clone()) {
                Some(mut path) => {
                    log::debug!("Following path {} to lambda", model::format_cmds(&path));
                    match sim::multistep(&current_state, &path) {
                        StepResult::Moved(new_state) => {
                            current_state = new_state;
                            lambdas_left = current_state.get_lambdas_coords();
                            robot_coord = current_state.get_pos();
                            current_path.append(&mut path);
                        },
                        StepResult::Ended(_) => panic!("GotoNearest: game was not supposed to end now ...")
                    }
                },
                None => {
                    log::debug!("GotoNearest: could not find path to lambda at {}", dst);
                    return None
                }
            }
        }

        log::debug!("GotoNearest: No more lambdas left to collect");

        let lift_solver = AStarGotoSolver::new(stop.clone(), current_state.get_lift_coord());
        match lift_solver.solve(current_state.clone()) {
            Some(mut path) => {
                log::debug!("Following path {} to lift", model::format_cmds(&path));
                current_path.append(&mut path);
            },
            None => {
                log::debug!("GotoNearest: could not find path to lift at {}", current_state.get_lift_coord());
                return None
            }
        }

        Some(current_path)
    }
}
