#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Coord {
    pub x: i32,
    pub y: i32,
}

impl Coord {
    pub fn neighbors(self) -> [Self; 4] {
        let x = self.x;
        let y = self.y;
        [
            Coord { x, y: y + 1 },
            Coord { x, y: y - 1 },
            Coord { x: x - 1, y },
            Coord { x: x + 1, y },
        ]
    }

    pub fn neighbors_with_action(self) -> [(Self, Dir); 4] {
        let x = self.x;
        let y = self.y;
        [
            (Coord { x, y: y + 1 }, Dir::Up),
            (Coord { x, y: y - 1 }, Dir::Down),
            (Coord { x: x - 1, y }, Dir::Left),
            (Coord { x: x + 1, y }, Dir::Right),
        ]
    }

    pub fn dist_to(self, other: Coord) -> i32 {
        (other.x - self.x).abs() + (other.y - self.y).abs()
    }
}

impl std::fmt::Display for Coord {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}, {})", self.x, self.y)?;

        Ok(())
    }
}

// Overloads the `+` operator for Coord + Coord and Coord + Dir.
impl<T> std::ops::Add<T> for Coord
where
    T: Into<Coord>,
{
    type Output = Self;

    fn add(self, other: T) -> Self {
        let c = other.into();
        Self {
            x: self.x + c.x,
            y: self.y + c.y,
        }
    }
}

// Overloads the `-` operator on Coord in terms of the `+` overload and the unary `-` overload.
impl<T> std::ops::Sub<T> for Coord
where
    T: std::ops::Neg,
    Self: std::ops::Add<<T as std::ops::Neg>::Output>,
{
    type Output = <Self as std::ops::Add<<T as std::ops::Neg>::Output>>::Output;

    fn sub(self, other: T) -> Self::Output {
        self + -other
    }
}

// Overloads the `+=` operator on Coord in terms of the `+` overload.
impl<T> std::ops::AddAssign<T> for Coord
where
    Coord: std::ops::Add<T, Output = Coord>,
{
    fn add_assign(&mut self, rhs: T) {
        *self = *self + rhs;
    }
}

// Overloads the `-=` operator on Coord in terms of the `-` overload.
impl<T> std::ops::SubAssign<T> for Coord
where
    Coord: std::ops::Sub<T, Output = Coord>,
{
    fn sub_assign(&mut self, rhs: T) {
        *self = *self - rhs;
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum Dir {
    Up,
    Down,
    Left,
    Right,
}
impl Dir {
    pub fn to_int(self) -> u8 {
        match self {
            Dir::Right => 0,
            Dir::Up => 1,
            Dir::Left => 2,
            Dir::Down => 3,
        }
    }

    pub fn from_int(i: u8) -> Self {
        match i % 4 {
            0 => Dir::Right,
            1 => Dir::Up,
            2 => Dir::Left,
            3 => Dir::Down,
            _ => panic!("impossible"),
        }
    }
}

impl From<Dir> for Coord {
    fn from(dir: Dir) -> Coord {
        match dir {
            Dir::Up => Coord { x: 0, y: 1 },
            Dir::Down => Coord { x: 0, y: -1 },
            Dir::Left => Coord { x: -1, y: 0 },
            Dir::Right => Coord { x: 1, y: 0 },
        }
    }
}

// Overloads the `+` operator for Dir + Coord.
impl std::ops::Add<Coord> for Dir {
    type Output = Coord;

    fn add(self, other: Coord) -> Coord {
        // Implemented in terms of Coord + Dir
        other + self
    }
}

// Overloads the unary `-` operator for Dir
impl std::ops::Neg for Dir {
    type Output = Dir;

    fn neg(self) -> Dir {
        match self {
            Dir::Right => Dir::Left,
            Dir::Left => Dir::Right,
            Dir::Up => Dir::Down,
            Dir::Down => Dir::Up,
        }
    }
}
